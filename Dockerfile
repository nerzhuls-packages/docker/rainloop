FROM alpine:3.18.4

WORKDIR /tmp
RUN apk add --no-cache wget unzip
RUN mkdir src && \
	wget https://github.com/RainLoop/rainloop-webmail/releases/download/v1.17.0/rainloop-legacy-1.17.0.zip && \
	unzip -d src rainloop-legacy-1.17.0.zip && find .

FROM php:8.1.24-apache
COPY --from=0 /tmp/src/ /var/www/html/
RUN apt-get update && apt-get install -y libpq-dev && docker-php-ext-install pdo pdo_pgsql && \
	apt-get clean && rm -rf /var/cache/apt/archives/* && \
	rm -rf /var/lib/apt/lists/*

RUN chown -R www-data:www-data /var/www/html/data
COPY ports.conf /etc/apache2/ports.conf
COPY virtualhost.conf /etc/apache2/sites-enables/000-default.conf

USER 1080:1080
EXPOSE 8080
